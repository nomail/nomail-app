package com.nomail.activite.ui.nouveauCourriel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.activite.ConnexionActivity;
import com.nomail.activite.NavDrawer;
import com.nomail.activite.ui.voirCourriel.FragmentVoirCourriel;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;
import com.nomail.model.courriel.CourrielRecu;

import org.json.JSONObject;


public class NouveauCourriel extends Fragment {
	final String TAG = "NouveauCourriel";
	Model model;
	IResultatObjet mResultatCallback;
	private int selectedPosition = -1;
	EditText destinataireField;
	EditText objetField;
	EditText messageField;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_nouveau_courriel, container, false);

		destinataireField = view.findViewById(R.id.edit_text_destinataire);
		objetField = view.findViewById(R.id.edit_text_objet);
		messageField = view.findViewById(R.id.edit_text_message);

		view.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.d(TAG, "onClick");
				String destinataire = destinataireField.getText().toString();
				String objet = objetField.getText().toString();
				String message = messageField.getText().toString();

				initVolleyCallbackNouveauCourriel();
				SharedPreferences sharedpreferences = requireContext().getSharedPreferences("NoMailCacheClient", 0);
				String token = sharedpreferences.getString("token", "");

				model = Model.getInstance(getActivity().getApplication(), getContext());
				model.postCourriel(token,destinataire,objet,message,mResultatCallback);
			}
		});
		OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {

			@Override
			public void handleOnBackPressed() {
				Intent nav = new Intent(getContext(), NavDrawer.class);
				startActivity(nav);
			}
		};

		requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
		return view;
	}

	void initVolleyCallbackNouveauCourriel() {
		Log.d(TAG, "initVolleyCallbackNouveauCourriel");
		mResultatCallback = new IResultatObjet() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
				Toast.makeText(getContext(), "Sent", Toast.LENGTH_LONG).show();
				destinataireField.getText().clear();
				objetField.getText().clear();
				messageField.getText().clear();
			}

			@Override
			public void surErreur(String requestType, VolleyError error) {
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(),R.string.erreurUtilisateurNonAutorise , Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}
}