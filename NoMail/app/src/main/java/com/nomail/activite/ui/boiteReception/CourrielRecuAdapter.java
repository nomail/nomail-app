package com.nomail.activite.ui.boiteReception;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.activite.ui.voirCourriel.FragmentVoirCourriel;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;
import com.nomail.model.courriel.CourrielRecu;

import org.json.JSONObject;

import java.util.ArrayList;


public class CourrielRecuAdapter extends RecyclerView.Adapter<CourrielRecuAdapter.CourrielRecuHolder> {

	String TAG = "CourrielRecuAdapter";

	private LayoutInflater inflater;
	private ArrayList<CourrielRecu> listeCourrielRecu;
	private int selectedPosition = -1;
	IResultatObjet mResultatCallbackObjet;
	Application application;
	Context context;
	ViewGroup parent;
	Model model;

	public CourrielRecuAdapter(Context ctx, Application app, ArrayList<CourrielRecu> listeCourrielRecu){
		application = app;
		inflater = LayoutInflater.from(ctx);
		context=ctx;
		if (listeCourrielRecu!=null) {
			this.listeCourrielRecu = listeCourrielRecu;
		} else {
			this.listeCourrielRecu= new ArrayList<>();
		}
	}

	@NonNull
	@Override
	public CourrielRecuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = inflater.inflate(R.layout.item_courriel, parent, false);
		this.parent=parent;
		return new CourrielRecuHolder(view);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onBindViewHolder(@NonNull CourrielRecuHolder holder, int position) {
		holder.constraint_layout_item_courriel.setBackgroundResource(R.drawable.un_courriel_dans_liste);

		holder.objet.setText(listeCourrielRecu.get(position).getObjet());
		holder.expediteur.setText(listeCourrielRecu.get(position).getExpediteur());
		holder.dateEnvoi.setText(listeCourrielRecu.get(position).getDateEnvoi());
	}

	@Override
	public int getItemCount() {
		return listeCourrielRecu.size();
	}

	class CourrielRecuHolder extends RecyclerView.ViewHolder{

		TextView objet, expediteur, dateEnvoi;
		ConstraintLayout constraint_layout_item_courriel;
		Button boutonArchiver, boutonSupprimer;

		public CourrielRecuHolder(View itemView) {
			super(itemView);

			context = itemView.getContext();

			objet = itemView.findViewById(R.id.lblItemCourrielObjet);
			expediteur = itemView.findViewById(R.id.lblItemCourrielExpediteur);
			dateEnvoi = itemView.findViewById(R.id.lblItemCourrielDateEnvoi);
			constraint_layout_item_courriel = itemView.findViewById(R.id.constraint_layout_item_courriel);
			boutonArchiver = itemView.findViewById(R.id.btnArchiver);
			boutonSupprimer = itemView.findViewById(R.id.btnSupprimer);

			itemView.setOnClickListener(new View.OnClickListener() {
				@RequiresApi(api = Build.VERSION_CODES.O)
				@Override
				public void onClick(View v) {
					selectedPosition = getAdapterPosition();

					FragmentVoirCourriel voirCourriel= new FragmentVoirCourriel();
					Bundle bundle = new Bundle();
					bundle.putString("idCourrielRecu", listeCourrielRecu.get(selectedPosition).getId());
					voirCourriel.setArguments(bundle);

					NavHostFragment navHostFragment =
							(NavHostFragment)((FragmentActivity)context).getSupportFragmentManager()
									.findFragmentById(R.id.nav_host_fragment);
					assert navHostFragment != null;
					NavController navController = navHostFragment.getNavController();
					navController.navigate(R.id.fragment_voir_courriel, bundle);

					notifyDataSetChanged();
				}
			});

			boutonArchiver.setOnClickListener(new View.OnClickListener() {
				@RequiresApi(api = Build.VERSION_CODES.O)
				@Override
				public void onClick(View v) {
					Toast.makeText(v.getContext(), context.getResources().getString(R.string.toast_courriel_archive), Toast.LENGTH_LONG).show();
					String token = context.getSharedPreferences("NoMailCacheClient", 0).getString("token", "");

					initVolleyCallbackPatchArchive();
					model = Model.getInstance(application, context);
					model.patchCourrielArchive(token, listeCourrielRecu.get(getAdapterPosition()).getId(), mResultatCallbackObjet);
					listeCourrielRecu.remove(getAdapterPosition());
					notifyDataSetChanged();
				}
			});

			boutonSupprimer.setOnClickListener(new View.OnClickListener() {
				@RequiresApi(api = Build.VERSION_CODES.O)
				@Override
				public void onClick(View v) {
					Toast.makeText(v.getContext(), context.getResources().getString(R.string.toast_courriel_supprime), Toast.LENGTH_LONG).show();
					String token = context.getSharedPreferences("NoMailCacheClient", 0).getString("token", "");

					initVolleyCallbackDeleteCourriel();
					model = Model.getInstance(application, context);
					model.deleteCourrielRecu(token, listeCourrielRecu.get(getAdapterPosition()).getId(), mResultatCallbackObjet);
					listeCourrielRecu.remove(getAdapterPosition());
					notifyDataSetChanged();
				}
			});
		}
	}

	void initVolleyCallbackPatchArchive() {
		Log.d(TAG, "initVolleyCallbackPatchArchive");
		mResultatCallbackObjet = new IResultatObjet() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);

				switch (statutHttp) {
					case 401:
						Toast.makeText(context, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(context, R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(context, R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	void initVolleyCallbackDeleteCourriel() {
		Log.d(TAG, "initVolleyCallbackDeleteCourriel");
		mResultatCallbackObjet = new IResultatObjet() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);

				switch (statutHttp) {
					case 401:
						Toast.makeText(context, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(context, R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(context, R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}
}
