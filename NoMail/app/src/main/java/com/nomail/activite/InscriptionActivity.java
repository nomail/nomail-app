package com.nomail.activite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;

import org.json.JSONObject;


public class InscriptionActivity extends AppCompatActivity {
	final String TAG = "InscriptionActivity";
	EditText editTextEmail, editTextPassword, editTextConfirmPassword, editTextPhraseDePasse;
	ProgressBar progressBar;
	IResultatObjet mResultatCallback;
	Model model;
	String courriel, motDePasse, phraseDePasse;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inscription);

		progressBar = findViewById(R.id.progressBar);

		editTextEmail = findViewById(R.id.editTextEmail);
		editTextPassword = findViewById(R.id.editTextPassword);
		editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
		editTextPhraseDePasse = findViewById(R.id.editTextPhraseDePasse);

		findViewById(R.id.buttonRegister).setOnClickListener(view -> {
				initVolleyCallbackRegister();
				register();

			});

		findViewById(R.id.textViewLogin).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
				startActivity(new Intent(InscriptionActivity.this, ConnexionActivity.class));
			}
		});
	}

	private void register() {
		 String email = editTextEmail.getText().toString().trim();
		 String password = editTextPassword.getText().toString().trim();
		 String confirmPassword = editTextConfirmPassword.getText().toString().trim();
		 phraseDePasse = editTextPhraseDePasse.getText().toString().trim();

		//Validations
		if (TextUtils.isEmpty(email)) {
			editTextEmail.setError("Saisir un courriel");
			editTextEmail.requestFocus();
			return;
		}

		if (TextUtils.isEmpty(password)) {
			editTextPassword.setError("Saisir un mot passe");
			editTextPassword.requestFocus();
			return;
		}

		if (password.length()<8) {
			editTextPassword.setError("Il faut huit caratères minimum");
			editTextPassword.requestFocus();
			return;
		}

		if (TextUtils.isEmpty(confirmPassword)) {
			editTextPassword.setError("Comfirmez le mot passe");
			editTextPassword.requestFocus();
			return;
		}

		if (!password.equals(confirmPassword)) {
			editTextConfirmPassword.setError("Les mots de passe doivent être identiques");
			editTextConfirmPassword.requestFocus();
			return;
		}

		if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			editTextEmail.setError("Entrez un courriel valide");
			editTextEmail.requestFocus();
			return;
		}

		if (TextUtils.isEmpty(phraseDePasse)) {
			editTextPassword.setError("Entrez une phrase de passe");
			editTextPassword.requestFocus();
			return;
		}
		if (phraseDePasse.length()<10) {
			editTextPassword.setError("Il faut dix caratères minimum");
			editTextPassword.requestFocus();
			return;
		}
		courriel=editTextEmail.getText().toString();
		motDePasse=editTextPassword.getText().toString();
		phraseDePasse=editTextPhraseDePasse.getText().toString();

		model = Model.getInstance(InscriptionActivity.this.getApplication(), this);
		model.register(courriel, motDePasse, phraseDePasse, mResultatCallback);
		};

		void login(){
			initVolleyCallbackConnexion();

			model = Model.getInstance(InscriptionActivity.this.getApplication(), this);
			model.login(courriel, motDePasse, phraseDePasse, mResultatCallback);
		}

		void initVolleyCallbackRegister(){
			//TextView lblErreur = findViewById(R.id.connexionLblErreur);
			SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("NoMailCacheClient", MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedpreferences.edit();

			mResultatCallback = new IResultatObjet() {
				@Override
				public void surSucces(String requestType, JSONObject response) {
					Toast.makeText(InscriptionActivity.this,"Yay", Toast.LENGTH_LONG).show();
					login();
				}

				@Override
				public void surErreur(String requestType, VolleyError error) {
					final int statutHttp = error.networkResponse.statusCode;
					Log.d(TAG, "Volley JSON post erreur");

					if (statutHttp == 401) {
						Toast.makeText(getApplicationContext(),"Erreur 401!", Toast.LENGTH_LONG).show();
					}
					else if(statutHttp == 400){
						editTextEmail.setError("Courriel déjà utilisé");
						editTextEmail.requestFocus();
					}
					else if (statutHttp == 404) {
						Toast.makeText(getApplicationContext(),"Erreur 404!", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(getApplicationContext(),"Erreur 469!", Toast.LENGTH_LONG).show();
					}
				}
			};
		}

		void initVolleyCallbackConnexion(){
			SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("NoMailCacheClient", MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedpreferences.edit();

			mResultatCallback = new IResultatObjet() {
				@Override
				public void surSucces(String requestType, JSONObject response) {
					Log.d(TAG, "Volley JSON post" + response);
					editor.putString("token", response.optString("token"));
					editor.apply();


					if (!response.optString("token").equals("")) {
						Intent bvn = new Intent(getApplicationContext(), BienvenueActivity.class);
						startActivity(bvn);
					} else {
						Toast.makeText(getApplicationContext(),"Connexion echouee", Toast.LENGTH_LONG).show();
					}
				}

				@Override
				public void surErreur(String requestType, VolleyError error) {
					final int statutHttp = error.networkResponse.statusCode;
					Log.d(TAG, "Volley JSON post erreur");
					Toast.makeText(getApplicationContext(),"Connexion echouee", Toast.LENGTH_LONG).show();
				}
			};
		}
	}
