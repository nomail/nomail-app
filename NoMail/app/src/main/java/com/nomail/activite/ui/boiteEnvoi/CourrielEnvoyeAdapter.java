package com.nomail.activite.ui.boiteEnvoi;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.activite.ui.voirCourriel.FragmentVoirCourriel;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;
import com.nomail.model.courriel.CourrielEnvoye;

import org.json.JSONObject;

import java.util.ArrayList;


public class CourrielEnvoyeAdapter extends RecyclerView.Adapter<CourrielEnvoyeAdapter.CourrielEnvoyeHolder> {

	final String TAG = "CourrielEnvoyeAdapter";

	private LayoutInflater inflater;
	private ArrayList<CourrielEnvoye> listeCourrielEnvoye;
	private int selectedPosition = -1;
	IResultatObjet mResultatCallbackObjet;
	Context context;
	Application application;
	ViewGroup parent;
	Model model;

	public CourrielEnvoyeAdapter(Context ctx, Application app, ArrayList<CourrielEnvoye> listeCourrielEnvoye){
		inflater = LayoutInflater.from(ctx);
		application = app;
		context=ctx;
		if (listeCourrielEnvoye!=null) {
			this.listeCourrielEnvoye = listeCourrielEnvoye;
		} else {
			this.listeCourrielEnvoye = new ArrayList<>();
		}
	}

	@NonNull
	@Override
	public CourrielEnvoyeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = inflater.inflate(R.layout.item_courriel, parent, false);
		this.parent=parent;
		return new CourrielEnvoyeAdapter.CourrielEnvoyeHolder(view);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onBindViewHolder(@NonNull CourrielEnvoyeAdapter.CourrielEnvoyeHolder holder, int position) {
		holder.constraint_layout_item_courriel.setBackgroundResource(R.drawable.un_courriel_dans_liste);

		holder.objet.setText(listeCourrielEnvoye.get(position).getObjet());
		holder.expediteur.setText(listeCourrielEnvoye.get(position).getDestinataire());
		holder.dateEnvoi.setText(listeCourrielEnvoye.get(position).getDateEnvoi());
	}

	@Override
	public int getItemCount() {
		return listeCourrielEnvoye.size();
	}

	class CourrielEnvoyeHolder extends RecyclerView.ViewHolder{

		TextView objet, expediteur, dateEnvoi;
		ConstraintLayout constraint_layout_item_courriel;
		Button boutonSupprimer;

		public CourrielEnvoyeHolder(View itemView) {
			super(itemView);

			context = itemView.getContext();

			objet = itemView.findViewById(R.id.lblItemCourrielObjet);
			expediteur = itemView.findViewById(R.id.lblItemCourrielExpediteur);
			dateEnvoi = itemView.findViewById(R.id.lblItemCourrielDateEnvoi);
			constraint_layout_item_courriel = itemView.findViewById(R.id.constraint_layout_item_courriel);
			boutonSupprimer = itemView.findViewById(R.id.btnSupprimer);

			itemView.findViewById(R.id.btnArchiver).setVisibility(View.GONE);

			itemView.setOnClickListener(new View.OnClickListener() {
				@RequiresApi(api = Build.VERSION_CODES.O)
				@Override
				public void onClick(View v) {
					selectedPosition = getAdapterPosition();

					FragmentVoirCourriel voirCourriel = new FragmentVoirCourriel();
					Bundle bundle = new Bundle();
					bundle.putString("idCourrielEnvoye", listeCourrielEnvoye.get(selectedPosition).getId());
					voirCourriel.setArguments(bundle);

					NavHostFragment navHostFragment =
							(NavHostFragment)((FragmentActivity)context).getSupportFragmentManager()
									.findFragmentById(R.id.nav_host_fragment);
					assert navHostFragment != null;
					NavController navController = navHostFragment.getNavController();
					navController.navigate(R.id.fragment_voir_courriel, bundle);

					notifyDataSetChanged();
				}
			});

			boutonSupprimer.setOnClickListener(new View.OnClickListener() {
				@RequiresApi(api = Build.VERSION_CODES.O)
				@Override
				public void onClick(View v) {
					Toast.makeText(v.getContext(), context.getResources().getString(R.string.toast_courriel_supprime), Toast.LENGTH_LONG).show();

					String token = context.getSharedPreferences("NoMailCacheClient", 0).getString("token", "");
					initVolleyCallbackDeleteCourriel();
					model = Model.getInstance(application, context);
					model.deleteCourrielEnvoye(token, listeCourrielEnvoye.get(getAdapterPosition()).getId(), mResultatCallbackObjet);
					listeCourrielEnvoye.remove(getAdapterPosition());
					notifyDataSetChanged();
				}
			});
		}
	}

	void initVolleyCallbackDeleteCourriel() {
		Log.d(TAG, "initVolleyCallbackDeleteCourriel");
		mResultatCallbackObjet = new IResultatObjet() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);

				switch (statutHttp) {
					case 401:
						Toast.makeText(context, R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(context, R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(context, R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

}