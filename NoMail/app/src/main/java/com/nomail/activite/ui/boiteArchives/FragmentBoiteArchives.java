package com.nomail.activite.ui.boiteArchives;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.nomail.BuildConfig;
import com.nomail.R;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatArray;
import com.nomail.model.courriel.CourrielRecu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class FragmentBoiteArchives extends Fragment {

	final String TAG = "FragmentBoiteArchives";

	private ProgressBar barreChargement;
	CourrielArchiveAdapter courrielArchiveAdapteur;
	RecyclerView recyclerView;

	ArrayList<CourrielRecu> courrielArchiveArrayList;
	IResultatArray mResultatCallbackArray;
	Model model;
	Context mContext;

	@RequiresApi(api = Build.VERSION_CODES.O)
	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		courrielArchiveAdapteur = new CourrielArchiveAdapter(getActivity(), requireActivity().getApplication(), courrielArchiveArrayList);
		View view = inflater.inflate(R.layout.fragment_boite_archive, container, false);

		recyclerView = view.findViewById(R.id.listeRVCourrielsArchives);

		showLoadingSpinner(view);

		initVolleyCallbackBoiteArchive();
		SharedPreferences sharedpreferences = requireContext().getSharedPreferences("NoMailCacheClient", 0);
		String token = sharedpreferences.getString("token", "");

		model = Model.getInstance(requireActivity().getApplication(), getContext());
		model.getCourrielsArchives(token, mResultatCallbackArray);
		return view;
	}

	void initVolleyCallbackBoiteArchive() {
		Log.d(TAG, "initVolleyCallbackBoiteArchive");
		mResultatCallbackArray = new IResultatArray() {
			@RequiresApi(api = Build.VERSION_CODES.O)
			@Override
			public void surSucces(String requestType, JSONArray response) {
				Log.d(TAG, "Volley JSON post" + response);
				removeLoadingSpinner();

				courrielArchiveArrayList = new ArrayList<>();
				JSONArray dataArray;
				try {
					dataArray = response;

					for (int i = 0; i < dataArray.length(); i++) {
						JSONObject dataobj = dataArray.getJSONObject(i);
						JSONObject objetCourriel = dataobj.getJSONObject("courriel");

						String objetDuCourriel = objetCourriel.getString("objet").length()> BuildConfig.TAILLE_MAX_OBJET_COURRIEL
								?objetCourriel.getString("objet").substring(0, BuildConfig.TAILLE_MAX_OBJET_COURRIEL)+"..."
								:objetCourriel.getString("objet");
						CourrielRecu courrielArchive = new CourrielRecu(
								dataobj.getString("_id"),
								dataobj.getString("lu"),
								dataobj.getString("archivé"),
								objetDuCourriel,
								new String(Base64.decode(objetCourriel.getString("adresseDestinataire"), Base64.DEFAULT), StandardCharsets.UTF_8),
								new String(Base64.decode(objetCourriel.getString("adresseExpéditeur"), Base64.DEFAULT), StandardCharsets.UTF_8),
								dataobj.getString("dateCréation")
						);

						courrielArchiveArrayList.add(courrielArchive);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				setupRecycler();
			}

			@Override
			public void surErreur (String requestType, VolleyError error){
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur: "+statutHttp);
				removeLoadingSpinner();

				switch (statutHttp) {
					case 401:
						Toast.makeText(getActivity(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
						break;
					case 404:
						Toast.makeText(getActivity(), R.string.erreurCourrielNonTrouve, Toast.LENGTH_LONG).show();
						break;
					default:
						Toast.makeText(getActivity(), R.string.erreurConnexion, Toast.LENGTH_LONG).show();
				}
			}
		};
	}

	private void setupRecycler(){
		recyclerView.setHasFixedSize(true);
		LinearLayoutManager llm = new LinearLayoutManager(mContext);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(llm);

		courrielArchiveAdapteur = new CourrielArchiveAdapter(getActivity(), requireActivity().getApplication(), courrielArchiveArrayList);
		recyclerView.setAdapter(courrielArchiveAdapteur);
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
	}

	public void removeLoadingSpinner() {
		try {
			if (barreChargement != null) {
				if (barreChargement.getVisibility()==View.VISIBLE) {
					barreChargement.setVisibility(View.GONE);
					barreChargement = null;
				}
			}
		} catch (Exception ie) {
			ie.printStackTrace();
		}
	}

	public void showLoadingSpinner(View view){
		try{
			if (barreChargement==null) {
				barreChargement = view.findViewById(R.id.progressBar_cyclic);
				barreChargement.setVisibility(View.VISIBLE);
				barreChargement.bringToFront();
			}

			if (!(barreChargement.getVisibility()==View.VISIBLE)) {
				barreChargement.setVisibility(View.VISIBLE);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}