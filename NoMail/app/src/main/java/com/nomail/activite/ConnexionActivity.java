package com.nomail.activite;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nomail.R;
import com.nomail.model.Model;
import com.nomail.model.api.IResultatObjet;

import org.json.JSONObject;


public class ConnexionActivity extends AppCompatActivity {
	final String TAG = "ConnexionActivity";
	IResultatObjet mResultatCallback;
	Model model;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connexion);

		Log.d(TAG, "OnCreate");

		EditText emailField = findViewById(R.id.editText_login_username);
		EditText passwordField = findViewById(R.id.editText_login_password);
		EditText pdpField = findViewById(R.id.editText_login_phrase_de_passe);

		Button loginBtn = findViewById(R.id.btn_connexion);
		loginBtn.setOnClickListener(view -> {
			Log.d(TAG, "OnClick");
			String courriel = emailField.getText().toString();
			String motDePasse = passwordField.getText().toString();
			String phraseDePasse = pdpField.getText().toString();

			initVolleyCallbackConnexion();

			model = Model.getInstance(ConnexionActivity.this.getApplication(), this);
			model.login(courriel, motDePasse, phraseDePasse, mResultatCallback);
		});

		Button inscriptionBtn = findViewById(R.id.btn_connexion_inscription);
		inscriptionBtn.setOnClickListener(view -> {
			Intent intent = new Intent(this, InscriptionActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		});

	}

	@Override
	public void onBackPressed() {
		Intent setIntent = new Intent(Intent.ACTION_MAIN);
		setIntent.addCategory(Intent.CATEGORY_HOME);
		setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(setIntent);
	}

	void initVolleyCallbackConnexion(){
		Log.d(TAG, "CallbackConnexion");
		TextView lblErreur = findViewById(R.id.connexionLblErreur);
		SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("NoMailCacheClient", MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedpreferences.edit();

		mResultatCallback = new IResultatObjet() {
			@Override
			public void surSucces(String requestType, JSONObject response) {
				Log.d(TAG, "Volley JSON post" + response);

				if (!response.optString("token").equals("")) {
					editor.putString("token", response.optString("token"));
					editor.apply();
					Intent nav = new Intent(getApplicationContext(), NavDrawer.class);
					startActivity(nav);
				} else {
					lblErreur.setText(R.string.erreurUtilisateurNonAutorise);
				}
			}

			@Override
			public void surErreur(String requestType, VolleyError error) {
				final int statutHttp = error.networkResponse.statusCode;
				Log.d(TAG, "Volley JSON post erreur");

				switch (statutHttp){
					case 401:
						lblErreur.setText(R.string.erreurUtilisateurNonAutorise);
						break;
					case 404:
						lblErreur.setText(R.string.erreurUtilisateurNonTrouve);
						break;
					default:
						lblErreur.setText(R.string.erreurConnexion);
				}
			}
		};
	}
}
