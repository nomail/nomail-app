package com.nomail.activite;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nomail.R;
import com.nomail.activite.ui.nouveauCourriel.NouveauCourriel;


public class NavDrawer extends AppCompatActivity {

	private AppBarConfiguration mAppBarConfiguration;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nav_drawer);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "New", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();

				new NouveauCourriel();
				NavHostFragment navHostFragment =
						(NavHostFragment)((FragmentActivity)view.getContext()).getSupportFragmentManager()
								.findFragmentById(R.id.nav_host_fragment);
				assert navHostFragment != null;
				NavController navController = navHostFragment.getNavController();
				navController.navigate(R.id.fragment_nouveau_courriel);
			}
		});

		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		mAppBarConfiguration = new AppBarConfiguration.Builder(
				R.id.nav_boite_reception, R.id.nav_boite_envoi, R.id.nav_boite_archives)
				.setDrawerLayout(drawer)
				.build();

		NavHostFragment navHostFragment =
				(NavHostFragment) getSupportFragmentManager()
						.findFragmentById(R.id.nav_host_fragment);
		assert navHostFragment != null;
		NavController navController = navHostFragment.getNavController();

		NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
		NavigationUI.setupWithNavController(navigationView, navController);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.nav_drawer, menu);
		MenuItem btnDeconnexion = menu.findItem(R.id.action_deconnexion);

		btnDeconnexion.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("NoMailCacheClient", MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedpreferences.edit();
				editor.clear();
				editor.apply();
				Intent connexion = new Intent(getApplicationContext(), ConnexionActivity.class);
				connexion.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(connexion);

				return true;
			}
		});
		return true;
	}

	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		return NavigationUI.navigateUp(navController, mAppBarConfiguration)
				|| super.onSupportNavigateUp();
	}
}