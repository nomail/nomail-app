package com.nomail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;


import com.android.volley.VolleyError;
import com.nomail.activite.ConnexionActivity;
import com.nomail.activite.NavDrawer;
import com.nomail.model.Model;
import android.os.Handler;
import com.nomail.model.api.IResultatObjet;

import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
	final String TAG = "MainActivity";
	IResultatObjet mResultatCallback;
	Model model;
	private AppBarConfiguration mAppBarConfiguration;
	private Handler mHandler;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		getSupportActionBar().hide();

		model = Model.getInstance(this.getApplication(), this);

		SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("NoMailCacheClient", MODE_PRIVATE);
		String token = sharedPreferences.getString("token", "");

		mHandler = new Handler(Looper.getMainLooper());
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (sharedPreferences.contains("token")) {
					initVolleyCallbackValider();
					model.validerToken(token, mResultatCallback);
				} else {
					Intent connexion = new Intent(getApplicationContext(), ConnexionActivity.class);
					connexion.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(connexion);
				}
			}
		}, 2000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
		return true;
	}

	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.fragment_container_view_tag);
		return NavigationUI.navigateUp(navController, mAppBarConfiguration)
				|| super.onSupportNavigateUp();
	}


	void initVolleyCallbackValider(){
		mResultatCallback = new IResultatObjet() {
			@Override
			public void surSucces(String requestType, JSONObject response) {
				if (response.optString("message").equals("Valide")) {
					Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.toast_main_utilisateur_connecte), Toast.LENGTH_LONG).show();
					// TODO: Essayer d'utiliser setContentView pour ne pas avoir un écran blanc lorsqu'on va en arrière
					Intent nav = new Intent(getApplicationContext(), NavDrawer.class);
					startActivity(nav);
				} else {
					Toast.makeText(getApplicationContext(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
					setContentView(R.layout.activity_connexion);
				}
			}

			@Override
			public void surErreur(String requestType, VolleyError error) {
				error.printStackTrace();
				Toast.makeText(getApplicationContext(), R.string.erreurUtilisateurNonAutorise, Toast.LENGTH_LONG).show();
				Intent connexion = new Intent(getApplicationContext(), ConnexionActivity.class);
				connexion.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(connexion);
			}
		};
	}
}
