package com.nomail.model.api;

public interface API {
	void register(String courriel, String motDePasse, String phraseDePasse,IResultatObjet callback);
	void login(String courriel, String motDePasse, String phraseDePasse, IResultatObjet callback);
	void validerToken(String token, IResultatObjet callback);
	void getCourrielsRecus(String token, IResultatArray callback);
	void getCourrielsEnvoyes(String token, IResultatArray callback);
	void getCourrielsArchives(String token, IResultatArray callback);
	void postCourriel(String token,String destinataire, String objet, String message,IResultatObjet callback);
	void getCourrielRecu(String token, String id, IResultatObjet callback);
	void getCourrielEnvoye(String token, String id, IResultatObjet callback);
	void getCourrielArchive(String token, String id, IResultatObjet callback);
	void patchCourrielArchive(String token, String id, IResultatObjet callback);
	void deleteCourrielRecu(String token, String id, IResultatObjet callback);
	void deleteCourrielEnvoye(String token, String id, IResultatObjet callback);
}
