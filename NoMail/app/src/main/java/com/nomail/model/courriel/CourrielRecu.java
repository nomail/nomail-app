package com.nomail.model.courriel;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class CourrielRecu {

	private Boolean estSelectionne;

	private String id, objet, expediteur, destinataire, dateEnvoi, contenu;
	private Boolean lu, archive;

	public CourrielRecu(String id, String lu, String archive, String objet, String expediteur, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.archive=Boolean.parseBoolean(archive);
		this.objet=objet;
		this.expediteur=expediteur;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("US/Eastern")).format(formatter):"";
	}

	public CourrielRecu(String id, String lu, String archive, String objet, String contenu, String expediteur, String destinataire, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.archive=Boolean.parseBoolean(archive);
		this.objet=objet;
		this.contenu=contenu;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("US/Eastern")).format(formatter):"";
	}

	public CourrielRecu(String id, String lu, String archive, String objet, String expediteur, String destinataire, String dateEnvoi){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		this.id=id;
		this.lu=Boolean.parseBoolean(lu);
		this.archive=Boolean.parseBoolean(archive);
		this.objet=objet;
		this.expediteur=expediteur;
		this.destinataire=destinataire;
		this.dateEnvoi= !dateEnvoi.equals("")?OffsetDateTime.parse(dateEnvoi).atZoneSimilarLocal(ZoneId.of("US/Eastern")).format(formatter):"";
	}

	public void setEstSelectionne(boolean selectionne){
		this.estSelectionne=selectionne;
	}

	public boolean estSelectionne(){
		return estSelectionne;
	}

	public String getId() {
		return id;
	}

	public Boolean getLu() {
		return lu;
	}

	public Boolean getArchive() {
		return archive;
	}

	public String getObjet() {
		return objet;
	}

	public String getContenu() {
		return contenu;
	}

	public String getExpediteur() {
		return expediteur;
	}

	public String getDestinataire() {
		return destinataire;
	}

	public String getDateEnvoi() {
		return dateEnvoi;
	}

	@Override
	public String toString() {
		return "CourrielRecu{" +
				"id='" + id + '\'' +
				", objet='" + objet + '\'' +
				", expediteur='" + expediteur + '\'' +
				", lu=" + lu +
				", archive=" + archive +
				", dateEnvoi=" + dateEnvoi +
				'}';
	}
}
